/*  Ecf-api-admin, 
    Béatrice <beaetfred76@gmail.com> 2019©
    It is an application that manages a promotion and students through a raw API and an admin interface. You can add, modify and delete promotions or students.
 */
    
It's a student management app
I used symfony

the installation doc is in a file that is called "ecf admin api bea.odt"

1- Install symfony
Go to https://symfony.com/doc/current/setup.html

$ composer create-project symfony / website-skeleton folder name

Check
$ php bin / console server: run

2-To create the API and admin I installed

$ composer require admin && composer require api

It will be necessary to modify the version in config / packages / doctrine.yaml

        server_version: '5.6'

3- In .env edit edit line 27
the user, pwd and name of the data bass.

4- Create the database

$ php bin / console doctrine: database: create

5 - Create the entities
Do not forget to validate embed in the API.

$ php bin / console make: entity

the table Promotion
Name / string / 255 and not nullable
startdate / Date
enddate / Date

We create the Student table
firstname / string / 255 and not nullable
lastname / string / 255 and not nullable

We will create the relationship between the two tables "ManyToOne"
We will return to the promotion table

$ php bin / console make: entity

put the name of the table
relationship
ManyToOne

6- Create the controller

$ php bin / console make: controller

Once the controller install and configure, do a migration
$ php bin / console make: migration
$ php bin / console do: mi: mi
put yes

7- Create user

$ php bin / console make: user

Save the changes in the DB:

$ php bin / console doctrine: migrations: diff
$ php bin / console doctrine: migrations: migrate

To go on the admin it is necessary to add / admin

8- creation of authenfication and form

$ php bin / console make: auth

9- Creating a form

$ php bin / console make: registration-form

validate and validate

10- go to / register

and creates an email address and password

Installation

Clone this repository to install the project :

$ git clone https://framagit.org/BECUE/symfony-ecf-api-admin-student.git

To run this project you have to install composer

$ composer install

To launch the project in your local browser use the command $ php bin/console server:run in the terminal and click on the link appearing below.

Then place /api at the end of the URL to access to the api or /admin to access to the app platform

http://127.0.0.1:8000/api
http://127.0.0.1:8000/admin

Available Scripts


C'est une application de gestion d'étudiants

j'ai utilisé symfony 

la doc d'installation est dans un fichier qui s'apelle "ecf admin api bea.odt"

1- Installer symfony
Aller sur https://symfony.com/doc/current/setup.html

$ composer create-project symfony/website-skeleton nom du dossier

Vérifier
$ php bin/console server:run

2-Pour créer l'api et admin j'ai installé

$ composer require admin  && composer require api

Il faudra modifier la version dans config/packages/doctrine.yaml

        server_version: '5.6'

3- Dans .env modification modifier la ligne 27
le user, pwd et nom de la basse de donnée.

4- Créer la base de donnée

$ php bin/console doctrine:database:create

5 - Créer les entités
Ne pas oublier de valider intégréer dans l'API.

$ php bin/console make:entity

la table Promotion
Name/string/255 et non nullable
startdate/date
enddate/date

On crée la table Student
firstname/string/255 et non nullable
lastname/string/255 et non nullable

On va crée la relation entre les deux tables "ManyToOne"
On va retourner dans la table promotion

$ php bin/console make:entity

mettre le nom de la table
relation
ManyToOne

6- Créer le controleur

php bin/console make:controller

Une fois le controller installer et paramettrer faire une migration
$ php bin/console make:migration
$ php bin/console do:mi:mi
mettre yes

7- Créer le user

$ php bin/console make:user

Sauvegardons les changements dans la BDD :

php bin/console doctrine:migrations:diff
php bin/console doctrine:migrations:migrate

Pour aller sur l'admin il faut rajouter /admin

8- création de l'authenfication et du formulaire

$ php bin/console make:auth

9- Création d'un formulaire

$ php bin/console make:registration-form

valider et valider

10- aller dans /register

et crée un adresse email et son mot de passe 

Pour accéder à l'apication en local

Ouvrir un teminal 
Taper cette commande

$ php bin/console server:run

Puis faire ctrl+cliquer sur url

Après le / mettre api si on veut accéder à api crud 

/ mettre admin si on veut accéder à l'admin et compléter les champs email et password
Pour installation

Pour utiliser de l'application il faut également installer composer

$ composer install




